/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Route from './src/routes/index';
import {Provider} from 'react-redux';
import store from './src/store';



const App = () => {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Route />
      </Provider>
    </NavigationContainer>
  );
}



export default App;
