import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {URL} from '../constants/constant';
axios.defaults.baseURL = URL.BASE_URL;

export default async function apiRequest(apiParams) {
  const token = await AsyncStorage.getItem('userToken');
  axios.defaults.headers.common['Authorization'] = token;
  try {
    const response = await axios(apiParams);
    return response;
  } catch (exception) {
    return exception;
  }
}
