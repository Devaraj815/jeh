import React, {useState} from 'react';
import {Text, StyleSheet, View, TextInput,Image, ImageBackground,Keyboard} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button } from 'react-native-elements';
import { ScreenName } from '../constants/constant';

import { useForm } from '../container/LoginContainer'

const bg = {uri:"https://www.themallodiham.co.uk/wp-content/uploads/2017/04/Hardware-stock-6.jpg"};
function Login({navigation}){
  const {values, errors, isSubmitting, handleChange, handleSubmit} = useForm(
    loginFunction,
  );

  function loginFunction() {
    navigation.reset({
      index: 0,
      routes: [{name: ScreenName.Dashboard}],
    });
  }
  const [icon, setIcon] = useState("eye-slash")
  const [hidePassword, setHidePassword] = useState(true)


  const _changeIcon = () => {
    icon !== "eye-slash"
      ? (setIcon("eye-slash"), setHidePassword(false))
      : (setIcon("eye"), setHidePassword(true))
  }
  // const [value, setValue] = useState(0);
  return (
  
    // <ImageBackground source={bg} style={styles.bg}>
    <View style={styles.container}>
    <View style={styles.fm}>
    <Image
        style={styles.image}
        source={require('../assets/logo.png')}
      />
      <Text style={styles.formLabel}> Login </Text>
      <View>
      {/* {errors.login && <Text style={styles.error}>{errors.login}</Text>} */}
      <View style={styles.SectionStyle}>
                    <Icon style={styles.InputIcon} name="mobile" size={21} color="#F79A70"/>
                <TextInput style={styles.textinput}  
                
                    placeholder="Enter Your Mobile"
                    placeholderTextColor = "#a7a7a7"
                    value={values.mobile}
                    onChangeText={(text) => {
                        handleChange(text, 'mobile');
                      }}
                    />
                 
             </View>
             {/* {errors.mobile && (
                <Text style={styles.error}>{errors.mobile}</Text>
              )} */}

      <View style={styles.SectionStyle}>
                    <Icon style={styles.InputIcon} name="unlock-alt" size={20} color="#F79A70"/>
                <TextInput style={styles.textinput}  
                    secureTextEntry={hidePassword}
              placeholder="Password"
              value={values.password}
              onChangeText={(text) => {
                handleChange(text, 'password');
              }}
              
            />
            <Icon style={styles.InputIcon} name={icon} size={12} onPress={() => _changeIcon()} />
                    

                   
              
             
              
                
            </View>
            {/* {errors.password && (
                <Text style={styles.error}>{errors.password}</Text>
              )} */}
            <View  style={styles.button} style={{top:'10%',}}>
            <Button buttonStyle={{borderRadius: 50, marginLeft: 0, marginRight: 0, marginBottom: 0 ,alignSelf:"center",width:150 }}
  title="Login"
  onPress={(e) => {
    Keyboard.dismiss();
    handleSubmit(e);
  }}
/>
</View>

        
      </View>
        

</View>
    </View>
    // </ImageBackground>
  
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#f7f8f9cc',
    alignItems: 'center',
    justifyContent: 'center',
   
    alignSelf: 'stretch',
    height: 50,

  },
  // bg: {
  //   flex: 1,
  //   // resizeMode: "cover",
  //   // justifyContent: "center",
  //   alignSelf: 'stretch',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
  fm: {

    backgroundColor: '#004e98',
    padding:50,
    alignItems: 'center',
    justifyContent: 'center',
    // borderRadius: 15,
    alignSelf: 'stretch',
    flex: 2,
   
  },

  image:{ 
    width: 150,
    height: 60 ,
    marginBottom: 20,
   },
   button: {
    borderRadius: 50 ,
   
},

  formLabel: {
    fontSize: 20,
    color: '#fcaf37',
  },
  inputStyle: {
    marginTop: 20,
    width: 300,
    height: 40,
    paddingHorizontal: 10,
    borderRadius: 50,
    backgroundColor: '#DCDCDC',
  },
  formText: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
    fontSize: 20,
  },
  butoonclass:{
    
    color: '#fff',
    backgroundColor: '#DCDCDC',
   marginTop: '10'
   
  },
  text: {
    color: '#fff',
    fontSize: 20,
  },
  fixToText:{
    top:'7%'
},

button: {
   
    backgroundColor: '#F79A70',
    borderColor: "transparent",
   
   justifyContent: 'center',
   alignItems: 'center',
   color:'#fff',
    borderRadius: 50 ,
},
  textinput: {
    flex:1,
    color: '#000',
    fontSize: 15,
 },
  
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: 1.5,
    borderColor: '#F79A70',
    borderRadius: 20 ,
    margin: 10,
    width:'80%'
},
InputIcon:{
    padding: 10,
    margin: 5,
    alignItems: 'center'
},
});

export default Login;