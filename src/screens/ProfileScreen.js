import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as action from '../action-reducer/action';
import { ScreenName } from '../constants/constant';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, View, StyleSheet,TextInput ,Image,ScrollView,Keyboard } from 'react-native';
import { Card,Button , CardItem, Thumbnail, Left, Body, Right, Header } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';



export default function Profile({ navigation: navigationRoute }){

      
    //    const { date,setState } = useState('15-05-2018')
       const {
        navigation,
        logout,
        Dashboard,
        Clientinfo,
        Profile
      } = useSelector((state) => state.data);

       const dispatch = useDispatch();
       
       const [values, setValues] = useState({});
       const [spinner,setSpinner] = useState(true); 

       const [fname,setFname] = useState('');
       const [lname,setlname] = useState('');
       const [mobile,setMobile] = useState('');
       const [dob,setDob] = useState('');
       const [cimage,setCimage] = useState('');
       const [email,setEmail] = useState('');
       const [address,setAddress] = useState('');
       const [errors, setErrors] = useState({});

       if(Profile){
       if(spinner){
        dispatch(action.getUserInfo());
        if(Array.isArray(Clientinfo) && Clientinfo.length ){
          console.log('Profile')
          let fname = Clientinfo[0].fname;
          let lname = Clientinfo[0].lname;
          let mobile = Clientinfo[0].mobile;
          let dob = Clientinfo[0].dob;
          let cimage = Clientinfo[0].cimage;
          let email = Clientinfo[0].email;
          
          let address = Clientinfo[0].address;
          setFname(fname)
          setlname(lname)
          setMobile(mobile)
          setDob(dob)
          setCimage(cimage)
          setEmail(email)
          setAddress(address)
          setSpinner(!spinner)
        }
       
      }
    }
      
      

        useEffect(() => {
             if (logout) {
                navigationRoute.navigate(ScreenName.login);
                //   dispatch(LoginaAction(false));
              }
             if (Dashboard) {
                navigationRoute.navigate(ScreenName.Dashboard);
              }

        })

        const  _card = el => {
            console.log('Card: ' + el.name)
        };
   
        const handleSubmit = (event) => {
            if (event) event.preventDefault();
            // console.log(values)
            dispatch(action.Dashboard(true));
        }

        //handelChanges
        const handleChange = (value, field) => {
            setErrors({});
            setValues(values => ({ ...values, [field]: value }));
        };
        
        // constructor(props) {
        //   super(props);
        //   this.state = {
        //     filePath: {},
        //   };
        // }


        // for choosefile
        const filePath= {};

        const chooseFile = () => {
          var options = {
            title: 'Select Image',
            customButtons: [
              { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            ],
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);
      
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
              alert(response.customButton);
            } else {
              let source = response;
              // You can also display the image using data:
              // let source = { uri: 'data:image/jpeg;base64,' + response.data };
              this.setState({
                filePath: source,
              });
            }
          });
        };


        
    return(
        <View style={styles.container}>
 
        <Header 
        
        statusBarProps={{ barStyle: 'light-content' }}
        
         containerStyle={{
           backgroundColor: 'red',
           justifyContent: 'space-around',
           paddingLeft:0,
         }}
       
       
         leftComponent={
          
           
       <View  style={{backgroundColor: '#fff', padding:3,}}>
           <Image
               style={styles.image}
               source={require('../assets/logo2.png')}
             />
       </View>
         }
         centerComponent={
           
            <View
           style={{ flexDirection: 'row' }}
       > 
          <Text style={{ flexShrink: 1,color:'#fff',paddingLeft:50 , fontWeight:'bold', fontStyle: 'italic',fontSize:14 }}>
          JAGADISH ELECTRICAL & HARDWARE
       
          </Text>
       </View> 
       
        }
         rightComponent={
             <TouchableOpacity onPress={() => {
                dispatch(action.Dashboard(true));
              }}>
                 <Icon  name="home" size={20} color="#fff"/>
             </TouchableOpacity>
           
         }
       
       />
       
       {/* header end */}
       
       
       <ScrollView style={styles.scrollView}>
       
       {/* card  profile*/}
       
       <View 
           style={{
            padding:0,marginTop:10
           }}
       >
       <View style={{  flex: 0,
        elevation: 3,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 1.0,
        marginVertical: 1,
        borderRadius: 2,
        marginHorizontal: 1,
        margin: 0,
        padding: 5,
        backgroundColor:'#fff',
       borderWidth: 0, }}>
       
         <Card.Title><Icon  name="user" size={20} color="#F79A70"/> Profile</Card.Title>
         <Card.Divider/>
       
       
       
       
         <View >
       
       {/* form start here */}
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
              First Name :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter Firstname"
                           placeholderTextColor = "#a7a7a7"
                           value ={fname}
                           onChangeText={(text) => {
                            setFname(text)
                            handleChange(text, 'fname');
                          }}
                           />
                           </View>
       
        
       
           </View>
       
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
              Last Name :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter LastName"
                           placeholderTextColor = "#a7a7a7"
                           value ={lname}
                           onChangeText={(text) => {
                            setlname(text)
                            handleChange(text, 'lname');
                          }}
                           />
                           </View>
       
        
       
           </View>
       
       <View>
       
       <View  >
               
               <Text style={{marginBottom: 10, }} >
              Date Of Birth :
         </Text>
         </View>
           <DatePicker 
                 style={{width: 200, 
          }}
                 date={dob} //initial date from state
                 mode="date" //The enum of date, datetime and time
                 placeholder="select date"
                 format="DD-MM-YYYY"
                 confirmBtnText="Confirm"
                 cancelBtnText="Cancel"
                 customStyles={{
                   dateIcon: {
                     position: 'absolute',
                     left: 0,
                     top: 4,
                     marginLeft: 0,
                     borderColor:'#ccc',
           borderRadius: 5, 
           borderWidth:1,
                   },
                   dateInput: {
                     marginLeft: 36
                   }
                 }}
                 onDateChange={(date) => {
                            setDob(date)                           
                    handleChange(date, 'dob');}}
               />
               </View>
       
               <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
              Mobile Number :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter MobileNumber"
                           placeholderTextColor = "#a7a7a7"
                           value ={mobile}
                           onChangeText={(text) => {
                            setMobile(text)
                            handleChange(text, 'mobile');
                          }}
                           />
                           
                           </View>
       
        
       
           </View>
       
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
              Email :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter Email"
                           placeholderTextColor = "#a7a7a7"
                           value ={email}
                           onChangeText={(text) => {
                            setEmail(text)
                            handleChange(text, 'email');
                          }}
                           />
                           </View>
       
        
       
           </View>
       
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
              Address :
         </Text>
         </View>
       
               
          <View style={{height: 70}} >
         <TextInput style={styles.textinput} 
                           multiline={true}
                           numberOfLines={5}
                           placeholder="Enter Address"
                           placeholderTextColor = "#a7a7a7"
                           value ={address}
                           onChangeText={(text) => {
                            setAddress(text) 
                            handleChange(text, 'address');
                          }}
                           />
                           </View>
        
        
       
           </View>

        
        {/* choose file */}
       
           <View style={styles.ccontainer}>
        <View style={styles.ccontainer}>

        <View >
               
               <Text style={{marginBottom: 10, }} >
              Profile Image :
         </Text>
         </View>
          {/*<Image 
          source={{ uri: this.state.filePath.path}} 
          style={{width: 100, height: 100}} />*/}
          <Image
            source={{
              uri: 'data:image/jpeg;base64,' + filePath.data,
            }}
            style={{ width: 10, height: 10 }}
          />
          <Image
            source={{ uri: filePath.uri }}
            style={{ width: 50, height: 10 }}
          />
          <Text style={{ alignItems: 'center' }}>
            {filePath.uri}
          </Text>
          <Button title="Choose File" onPress={chooseFile.bind(this)} />
        </View>
      </View>





           <View  style={styles.button}>
                   <Button buttonStyle={{borderRadius: 50, marginLeft: 0, marginRight: 0, marginBottom: 60 ,marginTop:10, backgroundColor:'orange',alignSelf:"center",width:150}}
         title="Save"
         onPress={(e) => {
            Keyboard.dismiss();
            handleSubmit(e);
          }}
       />
       </View>
       
       {/* form end here */}
       
          
       </View>
       
       </View>
       </View>
       
       
       </ScrollView>
       
       <View style={styles.bottomView}>
                 <Text style={{color:'#fff',}}> Support Number: 95005 49962 / 9489723691</Text>
                 <Text style={{color:'#fff',}}> Mail: sonurekha84@gmail.com</Text>
               </View>
       
       
             </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ecf0f1',
  
       
        alignSelf: 'stretch',
      
    
     
    },
    head: {
   backgroundColor:'orange',
    },
    textinput: {
      flex:1,
      height: 40,
      color: '#000',
      fontSize: 15,
      borderWidth:1,
      borderColor:'#ccc',
      borderRadius: 5,
      padding:5,
      marginBottom:10,
     
   },
    image:{ 
      width: 100,
      height: 50 ,
      // marginBottom: 20,
      // backgroundColor:'#fff',
      // marginLeft: -9,
     },
     bottomView: {
      width: '100%',
      height: 50,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
     
    },
    ccontainer: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
  });