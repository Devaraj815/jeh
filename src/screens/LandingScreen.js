import React, {useState, useEffect, useCallback} from 'react';
import { View,StyleSheet,Image} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Login from '../screens/LoginScreen';
import Dashboard from '../screens/DashboardScreen';

import { DotsLoader } from 'react-native-indicator';



function Landing(props){
    const [loading,setLoading] = useState(true);
    const [loginSuccess,setLoginSuccess] = useState(false);

    useEffect(() => {
        const validateToken = async () => {
          const onError = () => {
            setLoading(false);
            setLoginSuccess(false);
          };
          const onSuccess = () => {
            setLoading(false);
            setLoginSuccess(true);
          };
          await AsyncStorage.getItem('userToken').then((token) => {
            if (token) {
              onSuccess();
            } else {
              onError();
            }
          });
        };
        validateToken();
      }, []);


      const Element = loading ? ( <View style={styles.container}>
        <Image style={styles.image} 
          source={require("../assets/logo.png")}
        />
        <View style={{
            width:360,
            height:30,
            alignSelf:'auto',
            alignItems:'center',
            position:'absolute',
            bottom:280
        }}>
            <DotsLoader size={10} color='#F79A70' />
        </View>
    </View>  ) : loginSuccess ? ( <Dashboard { ...props } /> ) : ( <Login { ...props } />);

    return Element;
}


export default Landing;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#7579ff',
    },
    image:{
        marginTop:200,
        width:180,
        height:70,
    }
})