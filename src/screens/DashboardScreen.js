
import React,{ useEffect, useState } from 'react';
import { Text, View, StyleSheet ,Image,ScrollView} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import * as action from '../action-reducer/action';
import { ScreenName } from '../constants/constant';
import CardDashboard from './Card';
import { URL } from '../constants/constant';
import { Card,Button , CardItem, Thumbnail, Left, Body, Right, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Profile from './ProfileScreen';




export default function Dashboard({ navigation: navigationRoute }){
    const { navigation,  logout, Clientinfo,Price,Reward,Profile,Dashboard,history ,Bank,} = useSelector(
        (state) => state.data,
      );
  
   


    const [spinner,setSpinner] = useState(true); 
    const [name,setName] = useState(''); 
    const [mobile,setMobile] = useState(''); 
    const [thePrice,setThePrice] = useState(0); 
    const [theReward,setTheReward] = useState(0); 
    const [theImage,setImage] = useState(require('../assets/profile.png'))

    
    // AsyncStorage.getItem('userInfo').then(res => console.log(res))
    // console.log(AsyncStorage.getItem('userInfo'));
    
    const dispatch = useDispatch();
    const [isSubmitting, setIsSubmitting] = useState(false);

    
      if(spinner){
        dispatch(action.getUserInfo());
        if(Array.isArray(Clientinfo) && Clientinfo.length ){
          console.log('im in')
          let fnameAndLname = Clientinfo[0].fname+' '+Clientinfo[0].lname;
          let mobileNumber = Clientinfo[0].mobile;
          let BringImageOnServer = Clientinfo[0].cimage;
          let getPrice = Price;
          let getReard = Reward;
          setName(fnameAndLname)
          setMobile(mobileNumber)
          setThePrice(getPrice)
          setTheReward(getReard)
          let profile = {uri:URL.BASE_URL+'/'+BringImageOnServer}
          // console.log(getReard)
          setImage(profile)
          setSpinner(!spinner)
        }
       
      }
    
      

    useEffect(() => {
     
      
         if (logout) {
            setIsSubmitting(false);
            navigationRoute.navigate(ScreenName.login);
          }
          if (Profile) {
            navigationRoute.navigate(ScreenName.Profile);
          }
          if (Bank) {
            navigationRoute.navigate(ScreenName.Bank);
          }
          // console.log(History)
          if (history) {
            navigationRoute.navigate(ScreenName.History);
          }
    });


    const _card = el => {
        console.log('Card: ' + el.name)
      };

    
      const handleProfile = () =>{
        dispatch(action.Profile(true));
      }

      const handleBank = () =>{
        dispatch(action.Bank(true));
      }

      const handleHistory = () =>{
        console.log('his')
        dispatch(action.History(true));
      }
    
    return(
        <View style={styles.container}>
 
        
 <Header 
 
 statusBarProps={{ barStyle: 'light-content' }}
 
  containerStyle={{
    backgroundColor: 'red',
    justifyContent: 'space-around',
    paddingLeft:0,
  }}


  leftComponent={
   
    
<View  style={{backgroundColor: '#fff', padding:3,}}>
    <Image
        style={styles.image}
        source={require('../assets/logo2.png')}
      />
</View>
  }
  centerComponent={
    
     <View
    style={{ flexDirection: 'row' }}
> 
   <Text style={{ flexShrink: 1,color:'#fff',paddingLeft:50 , fontWeight:'bold', fontStyle: 'italic',fontSize:14 }}>
   JAGADISH ELECTRICAL & HARDWARE

   </Text>
</View> 

 }
         rightComponent={
            <TouchableOpacity onPress={() => {
                setIsSubmitting(true);
                dispatch(action.LogoutAction(true));
              }}>
              <Icon  name="sign-out" size={20} color="#fff"/>
            </TouchableOpacity> 
          
         }
       
       />
       
       
       
       <ScrollView style={styles.scrollView}>
       {/* header end */}
       
       {/* card */}
       <View 
           style={{
             borderColor:'#F46F1F', borderWidth:10,padding:0,marginTop:10
           }}
       >
       <View style={{  flex: 0,
        elevation: 3,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 1.0,
        marginVertical: 1,
        borderRadius: 2,
        marginHorizontal: 1,
        margin: 0,
        padding: 5,
        backgroundColor:'#fff',
       borderWidth: 0, }}>
       
         <View style={{flexDirection: 'column'}}>
         <View style={{flexDirection: 'row'}}>
               <View style={{flex:1,}} >
               
               <Image
               style={styles.image1}
               source={theImage}
             
             />
         </View>
       
               
         <View style={{flex:1,}} >
         <Text style={{color:'red', paddingTop: 13 , fontWeight:'bold',fontSize:20 }}>
       <Icon  name="user" size={20} color="#F79A70"/> {name}</Text>
         </View>
        
       
           </View>
           </View>
       
        
         
       
         
         
       
         <Card.Divider/>
       
         <View style={{flexDirection: 'column'}}>
           <View style={{flexDirection: 'row'}}>
               <View style={{flex:1,}} >
               
               <Text style={{marginBottom: 10, }} >
               <Icon  name="calendar" size={20} color="#F79A70"/> 01-10-2020
         </Text>
         </View>
       
               
         <View style={{flex:1,}} >
               
               <Text style={{marginBottom: 10, }} >
               <Icon  name="clock-o" size={20} color="#F79A70"/> 04:10:33
         </Text>
         </View>
         <View style={{flex:1,}} >
               
         <View  style={styles.button}>
                   
         <Text style={{marginBottom: 10, }} >
               <Icon  name="phone" size={20} color="#F79A70"/> {mobile}
               </Text>
       </View>
         </View>
       
           </View>
           <View style={{flexDirection: 'row'}}>
           <View style={{flex:1,}} >
               
               <Text style={{marginBottom: 10, }} >
               <Icon  name="money" size={20} color="#F79A70"/> Total Purchase 
         </Text>
         <Text style={{marginLeft: 20, }} >
         <Icon  name="inr" size={20} color="#0ac282"/> {thePrice} 
         </Text>
         </View>
       
               
         <View style={{flex:1, }} >
               
               <Text style={{marginBottom: 10, }} >
               <Icon  name="trophy" size={20} color="#F79A70"/> Total Reward
         </Text>
         <Text style={{marginLeft: 20, }} >
         <Icon  name="plus" size={20} color="#0ac282"/> {theReward} 
         </Text>
         </View>
           </View>
       </View>
       
       </View>
       </View>
       
       
       {/* dashboard cards */}
       
       <View>
              {/* card */}
               {/* block */}

               <View style={{flexDirection: 'row',top:'2%'}}>
      <View style={{flex:1,}} >
      <View style={styles.cardown}>
    <TouchableOpacity onPress={()=> handleProfile() }>
      <Image
        style={styles.image2}
        source={require('../assets/profile.png')}
      />
    </TouchableOpacity>
    <Text style={{alignSelf: 'center'}} >
    Profile
    </Text>
    </View>
    </View>
  
          
    <View style={{flex:1, }} >
    <View style={styles.cardown}>
         
         <TouchableOpacity onPress={()=> handleBank()} >
              <Image
              style={styles.image2}
              source={require('../assets/bank.png')}
            />
         </TouchableOpacity>
         
    
    <Text style={{alignSelf: 'center' }} >
   Bank Details 
    </Text>
    </View>
    </View>

    <View style={{flex:1,}} >
    <View style={styles.cardown}>
      <TouchableOpacity onPress={()=>handleHistory()} >
        <Image
            style={styles.image2}
            source={require('../assets/history.png')}
          />
        </TouchableOpacity>
        <Text style={{alignSelf: 'center' }} >
       History
  </Text>
  </View>
  </View>
      </View>

               {/* block */}
       
       </View>
       </ScrollView>
       
       
       <View style={styles.bottomView}>
          <Text style={{color:'#fff',}}> Support Number: 95005 49962 / 9489723691</Text>
          <Text style={{color:'#fff',}}> Mail: sonurekha84@gmail.com</Text>
        </View>

       
             </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ecf0f1',
  
       
        alignSelf: 'stretch',
      
    
     
    },
    head: {
   backgroundColor:'orange',
    },
    image:{ 
      width: 100,
      height: 50 ,
     },
     image1:{ 
      width: 50,
      height: 50 ,
      marginBottom: 10,
      justifyContent: 'center',
      
      alignSelf: 'center',
     
     },
     bottomView: {
      width: '100%',
      height: 50,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
     
    },

    cardown:{
      flex: 0,
      elevation: 3,
      shadowColor: "#000",
      shadowOpacity: 0.1,
      shadowOffset: { width: 0, height: 2 },
      shadowRadius: 1.0,
      marginVertical: 1,
      borderRadius: 5,
      marginHorizontal: 1,
      margin: 0,
      padding: 5,
      backgroundColor:'#fff',
     borderWidth: 0, 
     height: 100,
  
  
    },
    
    image2:{ 
      width: 50,
      height: 50 ,
      marginBottom: 10,
      justifyContent: 'center',
      
      alignSelf: 'center',
     },
  });