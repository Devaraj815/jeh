import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as action from '../action-reducer/action';
import { ScreenName } from '../constants/constant';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, View, StyleSheet,TextInput ,Image,ScrollView,Keyboard} from 'react-native';
import { Card,Button , CardItem, Thumbnail, Left, Body, Right, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import DocumentPicker from 'react-native-document-picker';
import Images from './Imagepicker';
import ImagePicker from 'react-native-image-picker'; // for choose file

export default function Bank({ navigation: navigationRoute }){


    const {
        navigation,
        logout,
        Dashboard,
        Clientinfo,
        Profile,
        Bank
      } = useSelector((state) => state.data);

      const dispatch = useDispatch();
      const [spinner,setSpinner] = useState(true);        
      const [values, setValues] = useState({});


      const [addhar,setAddhar] = useState('');
      const [bank,setBank] = useState('');
      const [accountnumber,setAccountnumber] = useState('');
      const [ifsc,setIfsc] = useState('');
      const [kyc,setKyc] = useState('');
      const [errors, setErrors] = useState({});

      if(Bank){
      if(spinner){
       dispatch(action.getUserInfo());
       if(Array.isArray(Clientinfo) && Clientinfo.length ){
         console.log('Bank')
         let addhar = Clientinfo[0].addhar;
         let bank = Clientinfo[0].bank;
         let accountnumber = Clientinfo[0].accountnumber;
         let ifsc = Clientinfo[0].ifsc;
         let kyc = Clientinfo[0].kyc;
  
         setAddhar(addhar)
         setBank(bank)
         setAccountnumber(accountnumber)
         setIfsc(ifsc)
         setKyc(kyc)
         setSpinner(!spinner)
       }
      
     }
   }
      
    //   console.log(spinner)
    //   setSpinner(false)
    //   console.log(spinner)
   
        //   if(spinner){
        //     dispatch(action.getUserInfo());
        //     console.log(Clientinfo)
        //     if(Array.isArray(Clientinfo) && Clientinfo.length ){
        //         console.log('pass')
        //         setSpinner(false)
        //         console.log(spinner)

        //     }
        //   }
 
       const handleSubmit = (event) => {
            if (event) event.preventDefault();
            // console.log(values)
            dispatch(action.Dashboard(true));
        }
      

      useEffect(() => {
        if (logout) {
           navigationRoute.navigate(ScreenName.login);
           //   dispatch(LoginaAction(false));
         }
        if (Dashboard) {
           navigationRoute.navigate(ScreenName.Dashboard);
         }

   })
   
//handelChanges
const handleChange = (value, field) => {
    setErrors({});
    setValues(values => ({ ...values, [field]: value }));
};


   const _card = el => {
    console.log('Card: ' + el.name)
  };



  // for choosefile
  const filePath= {};

  const chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
        });
      }
    });
  };

    return(
        <View style={styles.container}>
 
        <Header 
        
        statusBarProps={{ barStyle: 'light-content' }}
        
         containerStyle={{
           backgroundColor: 'red',
           justifyContent: 'space-around',
           paddingLeft:0,
         }}
       
       
         leftComponent={
          
           
       <View  style={{backgroundColor: '#fff', padding:3,}}>
           <Image
               style={styles.image}
               source={require('../assets/logo2.png')}
             />
       </View>
         }
         centerComponent={
           
            <View
           style={{ flexDirection: 'row' }}
       > 
          <Text style={{ flexShrink: 1,color:'#fff',paddingLeft:50 , fontWeight:'bold', fontStyle: 'italic',fontSize:14 }}>
          JAGADISH ELECTRICAL & HARDWARE
       
          </Text>
       </View> 
       
        }
         rightComponent={
            <TouchableOpacity onPress={() => {
                dispatch(action.Dashboard(true));
              }}>
           <Icon  name="home" size={20} color="#fff"/>
           </TouchableOpacity>
         }
       
       />
       
       {/* header end */}
       
       <ScrollView style={styles.scrollView}>
       
       {/* card  profile*/}
       
       <View 
           style={{
            padding:0,marginTop:10
           }}
       >
       <View style={{  flex: 0,
        elevation: 3,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 1.0,
        marginVertical: 1,
        borderRadius: 2,
        marginHorizontal: 1,
        margin: 0,
        padding: 5,
        backgroundColor:'#fff',
       borderWidth: 0, }}>
       
         <Card.Title><Icon  name="bank" size={20} color="#F79A70"/> Bank Details</Card.Title>
         <Card.Divider/>
       
       
       
       
         <View >
       
       {/* form start here */}
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
               Aadhar Number :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter Aadhar Number"
                           placeholderTextColor = "#a7a7a7"
                           value ={addhar}
                           onChangeText={(text) => {
                            setAddhar(text)
                            handleChange(text, 'addhar');
                          }}
                           />
                           </View>
       
        
       
           </View>
       
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
               Bank Name :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter Bank Name"
                           placeholderTextColor = "#a7a7a7"
                           value ={bank}
                           onChangeText={(text) => {
                            setBank(text)
                            handleChange(text, 'bank');
                          }}
                         
                           />
                           </View>
       
        
       
           </View>
       
       
       
               <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
               Accountnumber :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter Accountnumber"
                           placeholderTextColor = "#a7a7a7"
                           value ={accountnumber}
                           onChangeText={(text) => {
                            setAccountnumber(text)
                            handleChange(text, 'accountnumber');
                          }}
                         
                           />
                           </View>
       
        
       
           </View>
       
           <View >
               <View  >
               
               <Text style={{marginBottom: 10, }} >
               IFSC :
         </Text>
         </View>
       
               
          <View style={{height: 40}} >
         <TextInput style={styles.textinput}  
                           placeholder="Enter IFSC"
                           placeholderTextColor = "#a7a7a7"
                           value ={ifsc}
                           onChangeText={(text) => {
                            setifsc(text)
                            handleChange(text, 'ifsc');
                          }}
                         
                           />
                           </View>
       

       
           </View>
    
        {/* choose file */}
       
        <View style={styles.ccontainer}>
        <View style={styles.ccontainer}>

        <View >
               
               <Text style={{marginBottom: 10, }} >
              KYC Document :
         </Text>
         </View>
          {/*<Image 
          source={{ uri: this.state.filePath.path}} 
          style={{width: 100, height: 100}} />*/}
          <Image
            source={{
              uri: 'http://jagadishele.in' + filePath.data,
            }}
            style={{ width: 10, height: 10 }}
          />
          <Image
            source={{ uri: filePath.uri }}
            style={{ width: 50, height: 10 }}
          />
          <Text style={{ alignItems: 'center' }}>
            {filePath.uri}
          </Text>
          <Button title="Choose File" onPress={chooseFile.bind(this)} />
        </View>
      </View>
           
       
           <View  style={styles.button}>
                   <Button buttonStyle={{borderRadius: 50, marginLeft: 0, marginRight: 0, marginBottom: 20 ,marginTop:10, backgroundColor:'orange',alignSelf:"center",width:150}}
         title="Save"
         onPress={(e) => {
            Keyboard.dismiss();
            handleSubmit(e);
          }}
       />
       </View>
       
       {/* form end here */}
       
          
       </View>
       
       </View>
       </View>
       
       </ScrollView>
       
       <View style={styles.bottomView}>
                 <Text style={{color:'#fff',}}> Support Number: 95005 49962 / 9489723691</Text>
                 <Text style={{color:'#fff',}}> Mail: sonurekha84@gmail.com</Text>
               </View>
       
       
       
             </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ecf0f1',
  
       
        alignSelf: 'stretch',
      
    
     
    },
    head: {
   backgroundColor:'orange',
    },
    textinput: {
      flex:1,
      height: 40,
      color: '#000',
      fontSize: 15,
      borderWidth:1,
      borderColor:'#ccc',
      borderRadius: 5,
      padding:5,
      marginBottom:10,
     
   },
    image:{ 
      width: 100,
      height: 50 ,
      // marginBottom: 20,
      // backgroundColor:'#fff',
      // marginLeft: -9,
     },
  
     bottomView: {
      width: '100%',
      height: 50,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
     
    },
    ccontainer: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });