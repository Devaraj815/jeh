import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import * as action from '../action-reducer/action';
import { ScreenName } from '../constants/constant';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, View, StyleSheet,TextInput ,Image,ScrollView } from 'react-native';
import { Card,Button , CardItem, Thumbnail, Left, Body, Right, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { DataTable } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function History({ navigation: navigationRoute }){


    // constructor(props) {
    //     super(props)
    //     this.state = {
    //       page: 0,
    //       perPage: 5,
    //     }
    //   }
    
      const [statepage,setPage] = useState(0);
      const [stateperPage,setperPage] = useState(5);
      
     

      const data = [
        {
          sno:'1',
          date:'1-10-2020',
          purchase:'1000',
          reward:'2',
        },
        {
            sno:'2',
            date:'1-10-2020',
            purchase:'1000',
            reward:'2',
        },
        {
            sno:'3',
            date:'1-10-2020',
            purchase:'1000',
            reward:'2',
        },
       
      ];
  
      // end datas
  
      const page = 0;
      const perPage= 10;
      const from = page * perPage;
       const to = (page + 1) * perPage;

      const _card = el => {
        console.log('Card: ' + el.name)
      };


      
    const {
        navigation,
        logout,
        Dashboard,
        Clientinfo,
        Profile
      } = useSelector((state) => state.data);

      const dispatch = useDispatch();

      useEffect(() => {
        if (logout) {
           navigationRoute.navigate(ScreenName.login);
           //   dispatch(LoginaAction(false));
         }
        if (Dashboard) {
           navigationRoute.navigate(ScreenName.Dashboard);
         }

   })


   return(

    <View style={styles.container}>
      
 
      <Header 
 
 statusBarProps={{ barStyle: 'light-content' }}
 
  containerStyle={{
    backgroundColor: 'red',
    justifyContent: 'space-around',
    paddingLeft:0,
  }}


  leftComponent={
   
    
<View  style={{backgroundColor: '#fff', padding:3,}}>
    <Image
        style={styles.image}
        source={require('../assets/logo2.png')}
      />
</View>
  }
  centerComponent={
    
     <View
    style={{ flexDirection: 'row' }}
> 
   <Text style={{ flexShrink: 1,color:'#fff',paddingLeft:50 , fontWeight:'bold', fontStyle: 'italic',fontSize:14 }}>
   JAGADISH ELECTRICAL & HARDWARE

   </Text>
</View> 

 }
  rightComponent={
    <TouchableOpacity onPress={() => {
        dispatch(action.Dashboard(true));
      }}>
    <Icon  name="home" size={20} color="#fff"/>
    </TouchableOpacity>
  }

/>

{/* header end */}


<ScrollView style={styles.scrollView}>

{/* card  profile*/}

<View 
    style={{
     padding:0,marginTop:10
    }}
>
<View >

  <Card.Title><Icon  name="history" size={20} color="#F79A70"/> Overall History</Card.Title>
  <Card.Divider/>




  <View >

{/*datatable  start here */}
  



<DataTable>

<DataTable.Header>
<DataTable.Title>S.No</DataTable.Title>
<DataTable.Title numeric>Date</DataTable.Title>
<DataTable.Title numeric>Purchased</DataTable.Title>
<DataTable.Title numeric>Reward</DataTable.Title>
</DataTable.Header>

{data
  .slice(
    statepage * stateperPage,
    statepage * stateperPage + stateperPage,
  )
  .map(row => (
    <DataTable.Row>
<DataTable.Cell>{row.sno}</DataTable.Cell>
<DataTable.Cell numeric>{row.date}</DataTable.Cell>
<DataTable.Cell numeric>{row.purchase}</DataTable.Cell>
<DataTable.Cell numeric>{row.reward}</DataTable.Cell>
</DataTable.Row>



  ))}

<DataTable.Pagination
  page={statepage}
  numberOfPages={data.length / stateperPage}
  numberOfRows={data.length}
  perPage={stateperPage}
  onPageChange={page => setPage(page) }
  onChangeRowsPerPage={perPage => setperPage(perPage) }
  // possibleNumberPerPage={[2,3,4,5, 6]}
 
  label={`${from + 1}-${to} of ${data.length}`}
/>
</DataTable>
    
    



{/* datatable end here */}

   
</View>

</View>
</View>

</ScrollView>
<View style={styles.bottomView}>
          <Text style={{color:'#fff',}}> Support Number: 95005 49962 / 9489723691</Text>
          <Text style={{color:'#fff',}}> Mail: sonurekha84@gmail.com</Text>
        </View>

      </View>
   )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ecf0f1',
  
       
        alignSelf: 'stretch',
      
    
     
    },
    head: {
   backgroundColor:'orange',
    },
    textinput: {
      flex:1,
      height: 40,
      color: '#000',
      fontSize: 15,
      borderWidth:1,
      borderColor:'#ccc',
      borderRadius: 5,
      padding:5,
      marginBottom:10,
     
   },
    image:{ 
      width: 100,
      height: 50 ,
     
     },
     titile:{
      fontWeight:'bold',
      paddingBottom:10
     },
     bottomView: {
      width: '100%',
      height: 50,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
     
    },
  });