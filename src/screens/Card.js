import React, { Component ,useState} from 'react';
import { Text, View, StyleSheet ,Image} from 'react-native';
import { Card,Button , CardItem, Thumbnail, Left, Body, Right ,Header} from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
// import {
  
//     Header,
  
//   } from 'native-base';

  const CardDashboard = ({handleProfile}) => {
    
    return (


      <View style={{flexDirection: 'row',top:'2%'}}>
      <View style={{flex:1,}} >
      <View style={styles.cardown}>
    <TouchableOpacity onPress={()=> handleProfile() }>
      <Image
        style={styles.image}
        source={require('../assets/profile.png')}
      />
    </TouchableOpacity>
    <Text style={{alignSelf: 'center'}} >
    Profile
    </Text>
    </View>
    </View>
  
          
    <View style={{flex:1, }} >
    <View style={styles.cardown}>
         
          <Image
        style={styles.image}
        source={require('../assets/bank.png')}
      />
    
    <Text style={{alignSelf: 'center' }} >
   Bank Details 
    </Text>
    </View>
    </View>

    <View style={{flex:1,}} >
    <View style={styles.cardown}>
    <Image
        style={styles.image}
        source={require('../assets/history.png')}
      />
        <Text style={{alignSelf: 'center' }} >
       History
  </Text>
  </View>
  </View>
      </View>

);
};

const styles = StyleSheet.create({
  
  cardown:{
    flex: 0,
    elevation: 3,
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 1.0,
    marginVertical: 1,
    borderRadius: 5,
    marginHorizontal: 1,
    margin: 0,
    padding: 5,
    backgroundColor:'#fff',
   borderWidth: 0, 
   height: 100,


  },
  
  image:{ 
    width: 50,
    height: 50 ,
    marginBottom: 10,
    justifyContent: 'center',
    
    alignSelf: 'center',
   },

});

export default CardDashboard;