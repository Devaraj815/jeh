export const URL = {
    BASE_URL:'http://jagadishele.in',
    LOGIN:'/api/login',
    GET_USER:'/api/client/details/',
    GET_TOTAL_REWARD:'/api/client/total/'
}


export const ScreenName = {
    login: 'Login',
    Dashboard: 'Dashboard',
    landing:'Landing',
    Profile:'Profile',
    Bank:'Bank',
    History:'History'
  };