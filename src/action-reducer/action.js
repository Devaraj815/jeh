import axios from 'axios';
import apiRequest from '../serivce';
import * as constants from './constants';
import { URL } from '../constants/constant';
import AsyncStorage from '@react-native-community/async-storage';


export const LoginAction = (value) => {
    return async(dispatch) => {
       
        const response = await apiRequest({
            method:'POST',
            url:URL.BASE_URL+URL.LOGIN,
            data:value
        });
        let json = {}
        let type = constants.LOGIN;
        // console.log(response.data[0].token)
        console.log('im in action')
        if((response && response.data && response.data.invalid) || !response || !response.data ){
            json = response.data;
            type = constants.LOGIN_ERROR
        }else if(response && response.data && !response.data.invalid){
            
            json.id = response.data[0].id;
            json.token = response.data[0].token;
            json.fname = response.data[0].fname;
            json.lname = response.data[0].lname;
            json.mobile = response.data[0].mobile;
            await AsyncStorage.setItem('userToken',response.data[0].token);
            await AsyncStorage.setItem('userId',response.data[0].id);
            await AsyncStorage.setItem('userInfo',JSON.stringify(json));
            console.log(json)
            console.log('on login')
        }
        dispatch({
            type: type,
            payload: json,
          });
    }
}


export const LogoutAction = (value) => {
    return async (dispatch) => {
      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('userId');
     await AsyncStorage.removeItem('userInfo');
      dispatch({
        type: constants.LOGOUT,
        payload: value,
      });
    };
  };


export const getUserInfo = () =>{
  return async(dispatch) =>{
    const userId = await AsyncStorage.getItem('userId');
    console.log(userId)
    const response = await apiRequest({
      method:'GET',
      url:URL.BASE_URL+URL.GET_USER+userId,
  });
  let thejson = response.data;
  const responseReward = await apiRequest({
    method:'GET',
    url:URL.BASE_URL+URL.GET_TOTAL_REWARD+userId,
});
  console.log(responseReward.data)
  let Price = responseReward.data.price;
  let Reward = responseReward.data.reward;
  console.log(Reward)
  if(Price == null && Reward == null){
    Price = 0;
    Reward = 0;
  }
  dispatch({
    type: constants.UPDATE_USER,
    payload:{thejson,Price,Reward } 
  });
  
  }
}

export const Dashboard = (value) => {
  return async (dispatch) => {
    dispatch({
      type: constants.DASHBOARD,
      payload: value,
    });
  };
};

export const Profile = (value) => {
  return async (dispatch) => {
    dispatch({
      type: constants.PROFILE,
      payload: value,
    });
  };
};

export const Bank = (value) => {
  return async (dispatch) => {
    dispatch({
      type: constants.BANK,
      payload: value,
    });
  };
};

export const History = (value) => {
  return async (dispatch) => {
    dispatch({
      type: constants.HISTORY,
      payload: value,
    });
  };
};
