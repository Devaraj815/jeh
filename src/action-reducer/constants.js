export const LOGIN = 'LOGIN';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT = 'LOGOUT';
export const UPDATE_USER = 'UPDATE_USER';
export const DASHBOARD = 'DASHBOARD';
export const PROFILE = 'PROFILE';
export const BANK = 'BANK';
export const HISTORY = 'HISTORY';