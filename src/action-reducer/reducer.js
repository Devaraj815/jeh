import * as constants from './constants';

const initialState = {
    loginSuccess: false,
    loginError: false,
    navigation: false,
    logout: false,
    Dashboard: false,
    Profile:false,
    Bank:false,
    history:false,
    Clientinfo:[],
    Price:0,
    Reward:0,
  };

  export default function reducer(state = initialState, action) {
    switch (action.type) {
        case constants.LOGIN: {
            console.log('im in reducer')
            return {
              ...state,
              logout: false,
              navigation: false,
              loginError: false,
              loginSuccess: action.payload,
            };
          }
        case constants.LOGIN_ERROR: {
            return {
              ...state,
              logout: false,
              navigation: false,
              loginSuccess: false,
              Dashboard: false,
              Profile:false,
              Bank:false,
              history:false,
              loginError: action.payload,
            };
          }
          case constants.LOGOUT: {
            return {
              ...state,
              logout: action.payload,
              loginSuccess: false,
              loginError: false,
              navigation: false,
              Dashboard: false,
              Bank:false,
              history:false,
              Profile:false,
            };
          }
          case constants.UPDATE_USER:{
            console.log('price'+action.payload.Price)
            return{
              ...state,
              Clientinfo:action.payload.thejson,
              Price:action.payload.Price,
              Reward:action.payload.Reward,
            }
            
          }

          case constants.DASHBOARD: {
            return{
              ...state,
              logout: false,
              navigation: false,
              Dashboard: true,
              Profile:false,
              Bank:false,
              history:false,
            }
          }

          case constants.PROFILE: {
            return{
              ...state,
              logout: false,
              navigation: false,
              Dashboard: false,
              Profile:true,
              Bank:false,
              history:false,
            }
          }
          case constants.BANK: {
            return{
              ...state,
              logout: false,
              navigation: false,
              Dashboard: false,
              Profile:false,
              Bank:true,
              history:false,
            }
          }

          case constants.HISTORY: {
            console.log('on his')
            return{
              ...state,
              logout: false,
              navigation: false,
              Dashboard: false,
              Profile:false,
              Bank:false,
              history:true,
            }
          }


           default: {
               return state;
            }
    }
  }