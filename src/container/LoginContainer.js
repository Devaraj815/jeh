import { useSelector,useDispatch } from 'react-redux';
import * as action from '../action-reducer/action';
import { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export const validate = (values) => {
    let errors = {};
    if (!values.mobile) {
        errors.mobile = 'Mobile number is required';
        // } else if (/^\d{10}$/.test(values.mobile)) {
        //     errors.mobile = 'User Name is invalid';
    }
    if (!values.password) {
        errors.password = 'Password is required';
        // } else if (values.password.length < 8) {
        //     errors.password = 'Password must be 8 or more characters';
    }
    return errors;
};

const _retrieveUser = async () => {
    return await AsyncStorage.getItem('userToken');
}



export const useForm = (callback) => {
    const [values, setValues] = useState({});
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);
    const { loginSuccess, loginError } = useSelector((state) => state.data );
    const dispatch = useDispatch();
    
    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting){
            if (!errors.mobile && !errors.password) {
                if (loginSuccess) {
                    setValues({});
                    setErrors({});
                    setIsSubmitting(false);
                    callback();
                } else if (loginError && loginError.error) {
                    const errors = { login: 'Invalid Credential' };
                    setErrors(errors);
                    setIsSubmitting(false);
                    dispatch(action.LogoutAction(true));
                } else {
                    dispatch(action.LoginAction(values));
                }
            }
        }

        const asyncFunctionData = async () => {
            try {
                const token = await AsyncStorage.getItem('userToken');
                if (token) {
                    callback();
                }
            } catch (e) { }
        }

        asyncFunctionData();
    }, [errors, loginSuccess, loginError]);
    
    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        const errors = validate(values);
        setErrors(errors);
        if (errors.password || errors.mobile) {
            setIsSubmitting(false);
        } else {
            setIsSubmitting(true);
        }
    };

    const handleChange = (value, field) => {
        setErrors({});
        setValues(values => ({ ...values, [field]: value }));
    };

    return {
        handleChange,
        handleSubmit,
        isSubmitting,
        values,
        errors,
    }

}