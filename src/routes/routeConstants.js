import Login from '../screens/LoginScreen';
import Dashboard from '../screens/DashboardScreen';
import Landing from '../screens/LandingScreen';
import {ScreenName} from '../constants/constant';
import Profile from '../screens/ProfileScreen';
import Bank from '../screens/BankScreen';
import History from '../screens/HistoryScreen';

export default {
  initialRouteName: ScreenName.landing,
  routes: [
    {
      name: ScreenName.landing,
      component: Landing,
      isAuthenticated: false,
    },
    {
      name: ScreenName.login,
      component: Login,
      isAuthenticated: false,
    },
    {
      name: ScreenName.Dashboard,
      component: Dashboard,
      isAuthenticated: false,
    },
    {
      name:ScreenName.Profile,
      component:Profile,
      isAuthenticated: false,
    },
    {
      name:ScreenName.Bank,
      component:Bank,
      isAuthenticated: false,
    },
    {
      name:ScreenName.History,
      component:History,
      isAuthenticated: false,
    },
    
  ],
};
